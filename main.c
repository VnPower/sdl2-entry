#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_scancode.h>
#include <SDL2/SDL_timer.h>
#include <stdio.h>

#define WINDOW_HEIGHT (640)
#define WINDOW_WIDTH (480)

/* speed in pixels per second (px/s) */
#define SPEED (180)

/* global variables */
SDL_Window *win;
SDL_Renderer *rend;

typedef struct Texture_Object {
  float x;
  float y;
  float w;
  float h;
  SDL_Texture *tex;
  SDL_Rect rect;
} Texture_Object;

void die(char *msg) {
  fprintf(stderr, "%s: %s", msg, SDL_GetError());
  SDL_DestroyRenderer(rend);
  SDL_DestroyWindow(win);
  SDL_Quit();
  exit(1);
}

Texture_Object TXO_Create(char *image_path) {
  Texture_Object handler;

  SDL_Surface *surface = IMG_Load(image_path);
  if (!surface) {
    die("error creating surface");
  }

  SDL_Texture *tex = SDL_CreateTextureFromSurface(rend, surface);
  SDL_FreeSurface(surface);
  if (!tex) {
    die("error creating texture");
  }

  /* create a rectangle for the object */
  SDL_Rect dest;

  /* get and scale the dimensions of texture */
  SDL_QueryTexture(tex, 0, 0, &dest.w, &dest.h);

  handler.w = dest.w;
  handler.h = dest.h;
  handler.tex = tex;
  handler.rect = dest;

  return handler;
}

void TXO_UpdatePos(Texture_Object *obj) {
  obj->rect.x = obj->x;
  obj->rect.y = obj->y;
}

void TXO_CenterPos(Texture_Object *obj) {
  obj->x = (WINDOW_WIDTH - obj->w) / 2;
  obj->y = (WINDOW_HEIGHT - obj->h) / 2;
}

void TXO_Destroy(Texture_Object *obj) { SDL_DestroyTexture(obj->tex); }

int main(void) {
  /* attempt to initialize graphics and timer system */
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
    die("error initializing SDL");
  }

  win =
      SDL_CreateWindow("Hello, CS50!", SDL_WINDOWPOS_CENTERED,
                       SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, 0);
  if (!win) {
    die("error creating window");
  }

  Uint32 render_flags = SDL_RENDERER_ACCELERATED;
  rend = SDL_CreateRenderer(win, -1, render_flags);
  if (!rend) {
    die("error creating renderer");
  }

  Texture_Object gun = TXO_Create("resources/gun.png");
  Texture_Object ball = TXO_Create("resources/ball.png");
  TXO_CenterPos(&gun);
  TXO_CenterPos(&ball);
  /* the ball's position won't change until the user shoots the ball */
  TXO_UpdatePos(&ball);

  /* the angle of the gun */
  int angle = 0;

  /* give sprite initial velocity */
  float x_vel = 0;
  float y_vel = 0;

  /* handle close button */
  int close = 0;

  /* keep track of input */
  int left = 0;
  int right = 0;

  /* when the user shoots the ball */
  int shot = 0;
  int already_shot = 0;

  /* event loop */
  while (!close) {
    /* process events */
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_QUIT:
        close = 1;
        break;
      case SDL_KEYDOWN:
        switch (event.key.keysym.scancode) {
        case SDL_SCANCODE_Q:
          close = 1;
          break;
        case SDL_SCANCODE_LEFT:
          left = 1;
          break;
        case SDL_SCANCODE_RIGHT:
          right = 1;
          break;
        case SDL_SCANCODE_SPACE:
          shot = 1;
          break;
        default:
          /* to emit warnings */
          break;
        }
        break;
      case SDL_KEYUP:
        switch (event.key.keysym.scancode) {
        case SDL_SCANCODE_LEFT:
          left = 0;
          break;
        case SDL_SCANCODE_RIGHT:
          right = 0;
          break;
        default:
          /* to emit warnings */
          break;
        }
        break;
      }
    }

    if (left) {
      if (angle - 1 < 0)
        angle = 359;
      else
        angle--;
    }
    if (right) {
      if (angle + 1 > 360)
        angle = 1;
      else
        angle++;
    }

    if (shot && !already_shot) {
      int mod = angle % 180;
      if (0 <= angle && angle <= 90) {
        x_vel = angle;
        y_vel = angle - 90;
      }
      if (90 < angle && angle < 180) {
        x_vel = 90 + (90 - mod);
        y_vel = mod - 90;
      }
      if (180 <= angle && angle <= 270) {
        x_vel = -(angle - 180);
        y_vel = 90 - mod;
      }
      if (270 < angle && angle < 360) {
        x_vel = -(90 + (90 - mod));
        y_vel = -(mod - 90);
      }
      already_shot = 1;
    }

    if (already_shot) {
      /* collision detection */
      if (ball.x <= 0) {
        ball.x = 0;
        x_vel = -x_vel;
      }
      if (ball.y <= 0) {
        ball.y = 0;
        y_vel = -y_vel;
      }
      if (ball.x >= WINDOW_WIDTH - ball.w) {
        ball.x = WINDOW_WIDTH - ball.w;
        x_vel = -x_vel;
      }
      if (ball.y >= WINDOW_HEIGHT - ball.h) {
        ball.y = WINDOW_HEIGHT - ball.h;
        y_vel = -y_vel;
      }

      /* update positions
       * we divide the velocity if we want the ball to go slower
      */
      ball.x += x_vel / 5;
      ball.y += y_vel / 5;

      TXO_UpdatePos(&ball);
    }

    TXO_UpdatePos(&gun);

    /* clear the window */
    SDL_RenderClear(rend);

    /* draw the image to the window */
    SDL_RenderCopy(rend, ball.tex, 0, &ball.rect);
    SDL_RenderCopyEx(rend, gun.tex, 0, &gun.rect, angle, 0, 0);
    SDL_RenderPresent(rend);

    /* frame delay */
    SDL_Delay(1000 / 60);
  }

  /* clean up */
  TXO_Destroy(&gun);
  SDL_DestroyRenderer(rend);
  SDL_DestroyWindow(win);
  SDL_Quit();
}
